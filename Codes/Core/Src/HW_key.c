#include "HW_key.h"
uint8_t Key_row[1]={0xff};
/***
 *函数名：KEY_ROW_SCAN
 *功  能：按键行扫描
 *返回值：1~4，对应1~4行按键位置
 */
char KEY_ROW_SCAN(void)
{
    //读出行扫描状态
		Key_row[0] = HAL_GPIO_ReadPin(GPIOC,KEY_row4_Pin)<<4;
    Key_row[0] = Key_row[0] | (HAL_GPIO_ReadPin(GPIOA,KEY_row0_Pin)<<3);
    Key_row[0] = Key_row[0] | (HAL_GPIO_ReadPin(GPIOA,KEY_row1_Pin)<<2);
    Key_row[0] = Key_row[0] | (HAL_GPIO_ReadPin(GPIOA,KEY_row2_Pin)<<1);
    Key_row[0] = Key_row[0] | (HAL_GPIO_ReadPin(GPIOA,KEY_row3_Pin));
		
    if(Key_row[0] != 0x1f)         //行扫描有变化，判断该列有按键按下
    {
      HAL_Delay(10);                    //消抖
      if(Key_row[0] != 0x1f)
        {
                //printf("Key_Row_DATA = 0x%x\r\n",Key_row[0]);
                switch(Key_row[0])
                {
                    case 0x17:         //1 0111 判断为该列第1行的按键按下
                        return 1;
                    case 0x1b:         //1 1011 判断为该列第2行的按键按下
                        return 2;
                    case 0x1d:         //1 1101 判断为该列第3行的按键按下
                        return 3;
                    case 0x1e:         //1 1110 判断为该列第4行的按键按下
                        return 4;
										case 0x0f:				 //0 1111 判断为该列第5行的按键按下
												return 5;
                    default :
                        return 0;
                }
        }
        else return 0;
    }
    else return 0;
}
/***
 *函数名：KEY_SCAN
 *功  能：5*5按键扫描
 *返回值：0~25，对应25个按键
 */
char KEY_SCAN(void)
{
    char Key_Num=0;       //1-25对应的按键数
    char key_row_num=0;        //行扫描结果记录

    KEY_CLO0_OUT_LOW;
    if( (key_row_num=KEY_ROW_SCAN()) != 0 )
    {
        while(KEY_ROW_SCAN() != 0);  //消抖
        Key_Num = 0 + key_row_num;
        //printf("Key_Clo_1\r\n");
    }
    KEY_CLO0_OUT_HIGH;

    KEY_CLO1_OUT_LOW;
    if( (key_row_num=KEY_ROW_SCAN()) != 0 )
    {
        while(KEY_ROW_SCAN() != 0);
        Key_Num = 5 + key_row_num;
        //printf("Key_Clo_2\r\n");
    }
    KEY_CLO1_OUT_HIGH;

    KEY_CLO2_OUT_LOW;
    if( (key_row_num=KEY_ROW_SCAN()) != 0 )
    {
        while(KEY_ROW_SCAN() != 0);
				Key_Num = 10 + key_row_num;
        //printf("Key_Clo_3\r\n");
    }
    KEY_CLO2_OUT_HIGH;

    KEY_CLO3_OUT_LOW;
    if( (key_row_num=KEY_ROW_SCAN()) != 0 )
    {
        while(KEY_ROW_SCAN() != 0);
        Key_Num = 15 + key_row_num;
        //printf("Key_Clo_4\r\n");
    }
    KEY_CLO3_OUT_HIGH;
		
		KEY_CLO4_OUT_LOW;
    if( (key_row_num=KEY_ROW_SCAN()) != 0 )
    {
        while(KEY_ROW_SCAN() != 0);
        Key_Num = 20 + key_row_num;
        //printf("Key_Clo_5\r\n");
    }
    KEY_CLO4_OUT_HIGH;

    return Key_Num;
}
